//! Skeleton crate for a texdown to AST project.

#![allow(non_upper_case_globals)]

// Parser library.
#[macro_use]
extern crate nom ;
extern crate clap ;

use clap::{ Arg, App, SubCommand } ;

#[macro_use]
pub mod common ;
pub mod markers ;
pub mod ast ;
pub mod parser ;

pub mod to_texdown ;

macro_rules! try_exit {
  ($e:expr, failwith $( $blah:expr ),+) => (
    match $e {
      Ok(res) => res,
      Err(err) => {
        use std::process::exit ;
        println!("|===| Error") ;
        print!("| ") ;
        println!( $($blah),+ ) ;
        for line in format!("{}", err).lines() {
          println!("| {}", line)
        } ;
        println!("|===|") ;
        exit(2)
      }
    }
  ) ;
}

static input_key: & 'static str = "INPUT" ;
static output_key: & 'static str = "OUTPUT" ;
static verb_key: & 'static str = "VERBOSE" ;

static texdown_key: & 'static str = "texdown" ;
static html_key: & 'static str = "html" ;
static latex_key: & 'static str = "latex" ;
static texdown_dir: & 'static str = "texdown" ;
#[allow(dead_code)]
static html_dir: & 'static str = "html" ;
#[allow(dead_code)]
static latex_dir: & 'static str = "latex" ;

fn main() {
  use std::path::Path ;
  use std::fs::DirBuilder ;

  use ast::Frames ;

  // Building CLAP.
  let clap = App::new(
    "A TexDown translator to LaTeX and HTML."
  ).version("0.1.0").author(
    "Adrien Champion <adrien.champion@email.com>"
  ).arg(
    Arg::with_name(verb_key).short("v").multiple(true).help(
      "Activates verbosity (repeat to increase)"
    )
  ).arg(
    Arg::with_name(output_key).short("o").long("out").takes_value(true).help(
      "Output directory to write the files in"
    ).default_value(".")
  ).subcommand(
    SubCommand::with_name(texdown_key).about(
      "Options for texdown generation"
    ).author("Adrien Champion <adrien.champion@email.com>")
  ).subcommand(
    SubCommand::with_name(html_key).about(
      "Options for html    generation"
    ).author("The group")
  ).subcommand(
    SubCommand::with_name(latex_key).about(
      "Options for latex   generation"
    ).author("The group")
  ).arg(
    Arg::with_name(input_key).required(true).index(1).help(
      "Sets the input file to use"
    )
  ) ;

  // Parsing CLA.
  let matches = clap.get_matches() ;

  let source = matches.value_of(input_key).unwrap_or_else(
    || panic!("[CLAP] required input file not found")
  ) ;
  let target = matches.value_of(output_key).unwrap_or_else(
    || panic!("[CLAP] output dir not found (but has default value)")
  ) ;
  let verb = matches.occurrences_of(verb_key) ;

  verb_4!(
    verb, "\
|===| Top level configuration
| - input:            \"{}\"
| - target directory: \"{}\"
|
| - verbosity: {}
|===|
    ", source, target, verb
  ) ;

  let frames = try_exit!(
    Frames::of_file(& source, verb >= 2),
    failwith "while parsing input file \"{}\"", source
  ) ;
  verb_2!(verb) ;

  let file_name = match Path::new(source).file_name() {
    Some(file_name) => file_name,
    None => try_exit!(
      Err("could not retrieve name of source file"),
      failwith "preparing everything for translation"
    ),
  } ;
  let mut target_path = Path::new(target).to_path_buf() ;


  // Are we generating texdown?
  if let Some( matches ) = matches.subcommand_matches(texdown_key) {
    use to_texdown::ToTexdown ;
    verb_1!(verb, "|===| Generating texdown") ;
    target_path.push(texdown_dir) ;
    verb_1!(
      verb, "| base directory: \"{}\"",
      target_path.as_path().to_str().unwrap_or("<unknown>")
    ) ;
    // Creating directory if necessary.
    try_exit!(
      DirBuilder::new().recursive(true).create(target_path.as_path()),
      failwith "while creating texdown output directory"
    ) ;
    try_exit!(
      frames.to_texdown(file_name, target_path.as_path(), matches, verb),
      failwith "while generating texdown output"
    ) ;
    target_path.pop() ;
    verb_1!(verb, "|===| done") ;
  } ;

  // // Are we generating html?
  // if let Some( matches ) = matches.subcommand_matches(html_key) {
  //   use to_html::ToHtml ;
  //   target_path.push(html_dir) ;
  //   // Creating directory if necessary.
  //   try_exit!(
  //     DirBuilder::new().recursive(true).create(target_path.as_path()),
  //     failwith "while creating html output directory"
  //   ) ;
  //   try_exit!(
  //     frames.to_html(file_name, target_path.as_path(), matches),
  //     failwith "while generating html output"
  //   )
  // } ;

  // // Are we generating latex?
  // if let Some( matches ) = matches.subcommand_matches(latex_key) {
  //   use to_latex::ToLatex ;
  //   target_path.push(latex_dir) ;
  //   // Creating directory if necessary.
  //   try_exit!(
  //     DirBuilder::new().recursive(true).create(target_path.as_path()),
  //     failwith "while creating output directory"
  //   ) ;
  //   try_exit!(
  //     frames.to_latex(file_name, target_path.as_path(), matches),
  //     failwith "while generating texdown output"
  //   )
  // } ;
}