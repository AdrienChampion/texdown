//! Common things.

#[macro_export]
macro_rules! verb {
  ($v:expr, $lvl:expr, $( $body:expr ),+) => (
    if $v >= $lvl {
      for line in format!( $($body),+ ).lines() {
        println!("{}", line)
      }
    }
  ) ;
  ($v:expr, $lvl:expr) => ( verb!($v, $lvl, "") ) ;
}
#[macro_export]
macro_rules! verb_1 {
  ($v:expr, $( $body:expr ),+) => ( verb!($v, 1, $($body),+) ) ;
  ($v:expr) => ( verb!($v, 1) ) ;
}
#[macro_export]
macro_rules! verb_2 {
  ($v:expr, $( $body:expr ),+) => ( verb!($v, 2, $($body),+) ) ;
  ($v:expr) => ( verb!($v, 2) ) ;
}
#[macro_export]
macro_rules! verb_3 {
  ($v:expr, $( $body:expr ),+) => ( verb!($v, 3, $($body),+) ) ;
  ($v:expr) => ( verb!($v, 3) ) ;
}
#[macro_export]
macro_rules! verb_4 {
  ($v:expr, $( $body:expr ),+) => ( verb!($v, 4, $($body),+) ) ;
  ($v:expr) => ( verb!($v, 4) ) ;
}